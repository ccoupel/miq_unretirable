#
# Description: Placeholder for service request validation
#

$evm.root.attributes.each{|k,v| $evm.log(:info,"CC- ROOT[#{k}]=#{v}")}
#$evm.root["service_retire_request"].attributes.each{|k,v| $evm.log(:info,"CC- service_retire_request[#{k}]=#{v}")}
svcs=$evm.root["service_retire_request"]["options"][:src_ids].select{|id|
  svc=$evm.vmdb(:service,id)
  tagged=svc.tagged_with?("deletable","false")
  $evm.log(:info,"CC- #{id}: #{svc.name} tagged:#{tagged}")
  tagged

}

$evm.log(:info,"CC- #{svcs.count} not deletable svc=#{svcs}")
$evm.root["service_retire_request"].deny("admin","Retirement of this Service id no allowed") if svcs.count>0
exit MIQ_ABORT if svcs.count>0
